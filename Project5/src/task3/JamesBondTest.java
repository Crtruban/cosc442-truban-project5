package task3;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import java.util.regex.Pattern;

import org.hamcrest.Matcher;
import org.junit.Test;

public class JamesBondTest 
{
	JamesBond tester = new JamesBond();
	
	@Test
	public void testregexBond_0()
	throws Exception{
	assertFalse(tester.regexBond("( ("));
	}

	@Test
	public void testregexBond_1()
	throws Exception{
	assertFalse(tester.regexBond("( )"));
	}

	@Test
	public void testregexBond_2()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 )"));
	}

	@Test
	public void testregexBond_3()
	throws Exception{
	assertFalse(tester.regexBond("( 0 7 )"));
	}

	@Test
	public void testregexBond_4()
	throws Exception{
	assertFalse(tester.regexBond("( 7 )"));
	}

	@Test
	public void testregexBond_5()
	throws Exception{
	assertFalse(tester.regexBond("( ( ("));
	}

	@Test
	public void testregexBond_6()
	throws Exception{
	assertFalse(tester.regexBond("( ( )"));
	}

	@Test
	public void testregexBond_7()
	throws Exception{
	assertTrue(tester.regexBond("( ( 0 0 7 )"));
	}

	@Test
	public void testregexBond_8()
	throws Exception{
	assertFalse(tester.regexBond("( ( 0 7 )"));
	}

	@Test
	public void testregexBond_9()
	throws Exception{
	assertFalse(tester.regexBond("( ( 7 )"));
	}

	@Test
	public void testregexBond_10()
	throws Exception{
	assertFalse(tester.regexBond("( ) ("));
	}

	@Test
	public void testregexBond_11()
	throws Exception{
	assertFalse(tester.regexBond("( ) )"));
	}

	@Test
	public void testregexBond_12()
	throws Exception{
	assertTrue(tester.regexBond("( ) 0 0 7 )"));
	}

	@Test
	public void testregexBond_13()
	throws Exception{
	assertFalse(tester.regexBond("( ) 0 7 )"));
	}

	@Test
	public void testregexBond_14()
	throws Exception{
	assertFalse(tester.regexBond("( ) 7 )"));
	}

	@Test
	public void testregexBond_15()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ("));
	}

	@Test
	public void testregexBond_16()
	throws Exception{
	assertFalse(tester.regexBond("( 0 )"));
	}

	@Test
	public void testregexBond_17()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 0 7 )"));
	}

	@Test
	public void testregexBond_18()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ( ("));
	}

	@Test
	public void testregexBond_19()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ( )"));
	}

	@Test
	public void testregexBond_20()
	throws Exception{
	assertTrue(tester.regexBond("( 0 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond_21()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ( 0 7 )"));
	}

	@Test
	public void testregexBond_22()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ( 7 )"));
	}

	@Test
	public void testregexBond_23()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ) ("));
	}

	@Test
	public void testregexBond_24()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ) )"));
	}

	@Test
	public void testregexBond_25()
	throws Exception{
	assertTrue(tester.regexBond("( 0 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond_26()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ) 0 7 )"));
	}

	@Test
	public void testregexBond_27()
	throws Exception{
	assertFalse(tester.regexBond("( 0 ) 7 )"));
	}

	@Test
	public void testregexBond_28()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ("));
	}

	@Test
	public void testregexBond_29()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 )"));
	}

	@Test
	public void testregexBond_30()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 0 0 7 )"));
	}

	@Test
	public void testregexBond_31()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ( ("));
	}

	@Test
	public void testregexBond_32()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ( )"));
	}

	@Test
	public void testregexBond_33()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond_34()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ( 0 7 )"));
	}

	@Test
	public void testregexBond_35()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ( 7 )"));
	}

	@Test
	public void testregexBond_36()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ) ("));
	}

	@Test
	public void testregexBond_37()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ) )"));
	}

	@Test
	public void testregexBond_38()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond_39()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ) 0 7 )"));
	}

	@Test
	public void testregexBond_40()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 ) 7 )"));
	}

	@Test
	public void testregexBond_41()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 0 ("));
	}

	@Test
	public void testregexBond_42()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 0 )"));
	}

	@Test
	public void testregexBond_43()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 0 0 0 7 )"));
	}

	@Test
	public void testregexBond_44()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 1 ("));
	}

	@Test
	public void testregexBond_45()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 1 )"));
	}

	@Test
	public void testregexBond_46()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 1 0 0 7 )"));
	}

	@Test
	public void testregexBond_47()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 1 0 7 )"));
	}

	@Test
	public void testregexBond_48()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 1 7 )"));
	}

	@Test
	public void testregexBond_49()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 2 ("));
	}

	@Test
	public void testregexBond_50()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 2 )"));
	}

	@Test
	public void testregexBond_51()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 2 0 0 7 )"));
	}

	@Test
	public void testregexBond_52()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 2 0 7 )"));
	}

	@Test
	public void testregexBond_53()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 2 7 )"));
	}

	@Test
	public void testregexBond_54()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 3 ("));
	}

	@Test
	public void testregexBond_55()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 3 )"));
	}

	@Test
	public void testregexBond_56()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 3 0 0 7 )"));
	}

	@Test
	public void testregexBond_57()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 3 0 7 )"));
	}

	@Test
	public void testregexBond_58()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 3 7 )"));
	}

	@Test
	public void testregexBond_59()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 4 ("));
	}

	@Test
	public void testregexBond_60()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 4 )"));
	}

	@Test
	public void testregexBond_61()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 4 0 0 7 )"));
	}

	@Test
	public void testregexBond_62()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 4 0 7 )"));
	}

	@Test
	public void testregexBond_63()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 4 7 )"));
	}

	@Test
	public void testregexBond_64()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 5 ("));
	}

	@Test
	public void testregexBond_65()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 5 )"));
	}

	@Test
	public void testregexBond_66()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 5 0 0 7 )"));
	}

	@Test
	public void testregexBond_67()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 5 0 7 )"));
	}

	@Test
	public void testregexBond_68()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 5 7 )"));
	}

	@Test
	public void testregexBond_69()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 6 ("));
	}

	@Test
	public void testregexBond_70()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 6 )"));
	}

	@Test
	public void testregexBond_71()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 6 0 0 7 )"));
	}

	@Test
	public void testregexBond_72()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 6 0 7 )"));
	}

	@Test
	public void testregexBond_73()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 6 7 )"));
	}

	@Test
	public void testregexBond_74()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 ("));
	}

	@Test
	public void testregexBond_75()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 0 0 7 )"));
	}

	@Test
	public void testregexBond_76()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 0 7 )"));
	}

	@Test
	public void testregexBond_77()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 7 )"));
	}

	@Test
	public void testregexBond_78()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 ( ("));
	}

	@Test
	public void testregexBond_79()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ( )"));
	}

	@Test
	public void testregexBond_80()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ( 0 0 7 )"));
	}

	@Test
	public void testregexBond_81()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ( 0 7 )"));
	}

	@Test
	public void testregexBond_82()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ( 7 )"));
	}

	@Test
	public void testregexBond_83()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ("));
	}

	@Test
	public void testregexBond_84()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) )"));
	}

	@Test
	public void testregexBond_85()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 0 0 7 )"));
	}

	@Test
	public void testregexBond_86()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 0 7 )"));
	}

	@Test
	public void testregexBond_87()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 7 )"));
	}

	@Test
	public void testregexBond_88()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ( ("));
	}

	@Test
	public void testregexBond_89()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ( )"));
	}

	@Test
	public void testregexBond_90()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ( 0 0 7 )"));
	}

	@Test
	public void testregexBond_91()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ( 0 7 )"));
	}

	@Test
	public void testregexBond_92()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ( 7 )"));
	}

	@Test
	public void testregexBond_93()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ) ("));
	}

	@Test
	public void testregexBond_94()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ) )"));
	}

	@Test
	public void testregexBond_95()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ) 0 0 7 )"));
	}

	@Test
	public void testregexBond_96()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ) 0 7 )"));
	}

	@Test
	public void testregexBond_97()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) ) 7 )"));
	}

	@Test
	public void testregexBond_98()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 0 ("));
	}

	@Test
	public void testregexBond_99()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 0 )"));
	}

	@Test
	public void testregexBond_100()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 0 0 0 7 )"));
	}

	@Test
	public void testregexBond_101()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 1 ("));
	}

	@Test
	public void testregexBond_102()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 1 )"));
	}

	@Test
	public void testregexBond_103()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 1 0 0 7 )"));
	}

	@Test
	public void testregexBond_104()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 1 0 7 )"));
	}

	@Test
	public void testregexBond_105()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 1 7 )"));
	}

	@Test
	public void testregexBond_106()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 2 ("));
	}

	@Test
	public void testregexBond_107()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 2 )"));
	}

	@Test
	public void testregexBond_108()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 2 0 0 7 )"));
	}

	@Test
	public void testregexBond_109()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 2 0 7 )"));
	}

	@Test
	public void testregexBond_110()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 2 7 )"));
	}

	@Test
	public void testregexBond_111()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 3 ("));
	}

	@Test
	public void testregexBond_112()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 3 )"));
	}

	@Test
	public void testregexBond_113()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 3 0 0 7 )"));
	}

	@Test
	public void testregexBond_114()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 3 0 7 )"));
	}

	@Test
	public void testregexBond_115()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 3 7 )"));
	}

	@Test
	public void testregexBond_116()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 4 ("));
	}

	@Test
	public void testregexBond_117()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 4 )"));
	}

	@Test
	public void testregexBond_118()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 4 0 0 7 )"));
	}

	@Test
	public void testregexBond_119()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 4 0 7 )"));
	}

	@Test
	public void testregexBond_120()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 4 7 )"));
	}

	@Test
	public void testregexBond_121()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 5 ("));
	}

	@Test
	public void testregexBond_122()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 5 )"));
	}

	@Test
	public void testregexBond_123()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 5 0 0 7 )"));
	}

	@Test
	public void testregexBond_124()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 5 0 7 )"));
	}

	@Test
	public void testregexBond_125()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 5 7 )"));
	}

	@Test
	public void testregexBond_126()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 6 ("));
	}

	@Test
	public void testregexBond_127()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 6 )"));
	}

	@Test
	public void testregexBond_128()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 6 0 0 7 )"));
	}

	@Test
	public void testregexBond_129()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 6 0 7 )"));
	}

	@Test
	public void testregexBond_130()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 6 7 )"));
	}

	@Test
	public void testregexBond_131()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 7 ("));
	}

	@Test
	public void testregexBond_132()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 7 0 0 7 )"));
	}

	@Test
	public void testregexBond_133()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 7 0 7 )"));
	}

	@Test
	public void testregexBond_134()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 7 7 )"));
	}

	@Test
	public void testregexBond_135()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 8 ("));
	}

	@Test
	public void testregexBond_136()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 8 )"));
	}

	@Test
	public void testregexBond_137()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 8 0 0 7 )"));
	}

	@Test
	public void testregexBond_138()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 8 0 7 )"));
	}

	@Test
	public void testregexBond_139()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 8 7 )"));
	}

	@Test
	public void testregexBond_140()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 9 ("));
	}

	@Test
	public void testregexBond_141()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 9 )"));
	}

	@Test
	public void testregexBond_142()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 9 0 0 7 )"));
	}

	@Test
	public void testregexBond_143()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 9 0 7 )"));
	}

	@Test
	public void testregexBond_144()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 ) 9 7 )"));
	}

	@Test
	public void testregexBond_145()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 0 ("));
	}

	@Test
	public void testregexBond_146()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 0 )"));
	}

	@Test
	public void testregexBond_147()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 0 0 0 7 )"));
	}

	@Test
	public void testregexBond_148()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 1 ("));
	}

	@Test
	public void testregexBond_149()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 1 )"));
	}

	@Test
	public void testregexBond_150()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 1 0 0 7 )"));
	}

	@Test
	public void testregexBond_151()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 1 0 7 )"));
	}

	@Test
	public void testregexBond_152()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 1 7 )"));
	}

	@Test
	public void testregexBond_153()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 2 ("));
	}

	@Test
	public void testregexBond_154()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 2 )"));
	}

	@Test
	public void testregexBond_155()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 2 0 0 7 )"));
	}

	@Test
	public void testregexBond_156()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 2 0 7 )"));
	}

	@Test
	public void testregexBond_157()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 2 7 )"));
	}

	@Test
	public void testregexBond_158()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 3 ("));
	}

	@Test
	public void testregexBond_159()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 3 )"));
	}

	@Test
	public void testregexBond_160()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 3 0 0 7 )"));
	}

	@Test
	public void testregexBond_161()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 3 0 7 )"));
	}

	@Test
	public void testregexBond_162()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 3 7 )"));
	}

	@Test
	public void testregexBond_163()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 4 ("));
	}

	@Test
	public void testregexBond_164()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 4 )"));
	}

	@Test
	public void testregexBond_165()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 4 0 0 7 )"));
	}

	@Test
	public void testregexBond_166()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 4 0 7 )"));
	}

	@Test
	public void testregexBond_167()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 4 7 )"));
	}

	@Test
	public void testregexBond_168()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 5 ("));
	}

	@Test
	public void testregexBond_169()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 5 )"));
	}

	@Test
	public void testregexBond_170()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 5 0 0 7 )"));
	}

	@Test
	public void testregexBond_171()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 5 0 7 )"));
	}

	@Test
	public void testregexBond_172()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 5 7 )"));
	}

	@Test
	public void testregexBond_173()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 6 ("));
	}

	@Test
	public void testregexBond_174()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 6 )"));
	}

	@Test
	public void testregexBond_175()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 6 0 0 7 )"));
	}

	@Test
	public void testregexBond_176()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 6 0 7 )"));
	}

	@Test
	public void testregexBond_177()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 6 7 )"));
	}

	@Test
	public void testregexBond_178()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 7 ("));
	}

	@Test
	public void testregexBond_179()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 7 0 0 7 )"));
	}

	@Test
	public void testregexBond_180()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 7 0 7 )"));
	}

	@Test
	public void testregexBond_181()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 7 7 )"));
	}

	@Test
	public void testregexBond_182()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 8 ("));
	}

	@Test
	public void testregexBond_183()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 8 )"));
	}

	@Test
	public void testregexBond_184()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 8 0 0 7 )"));
	}

	@Test
	public void testregexBond_185()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 8 0 7 )"));
	}

	@Test
	public void testregexBond_186()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 8 7 )"));
	}

	@Test
	public void testregexBond_187()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 7 9 ("));
	}

	@Test
	public void testregexBond_188()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 9 )"));
	}

	@Test
	public void testregexBond_189()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 9 0 0 7 )"));
	}

	@Test
	public void testregexBond_190()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 9 0 7 )"));
	}

	@Test
	public void testregexBond_191()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 7 9 7 )"));
	}

	@Test
	public void testregexBond_192()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 8 ("));
	}

	@Test
	public void testregexBond_193()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 8 )"));
	}

	@Test
	public void testregexBond_194()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 8 0 0 7 )"));
	}

	@Test
	public void testregexBond_195()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 8 0 7 )"));
	}

	@Test
	public void testregexBond_196()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 8 7 )"));
	}

	@Test
	public void testregexBond_197()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 9 ("));
	}

	@Test
	public void testregexBond_198()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 9 )"));
	}

	@Test
	public void testregexBond_199()
	throws Exception{
	assertTrue(tester.regexBond("( 0 0 9 0 0 7 )"));
	}

	@Test
	public void testregexBond_200()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 9 0 7 )"));
	}

	@Test
	public void testregexBond_201()
	throws Exception{
	assertFalse(tester.regexBond("( 0 0 9 7 )"));
	}

	@Test
	public void testregexBond_202()
	throws Exception{
	assertFalse(tester.regexBond("( 0 1 ("));
	}

	@Test
	public void testregexBond_203()
	throws Exception{
	assertFalse(tester.regexBond("( 0 1 )"));
	}

	@Test
	public void testregexBond_204()
	throws Exception{
	assertTrue(tester.regexBond("( 0 1 0 0 7 )"));
	}

	@Test
	public void testregexBond_205()
	throws Exception{
	assertFalse(tester.regexBond("( 0 1 0 7 )"));
	}

	@Test
	public void testregexBond_206()
	throws Exception{
	assertFalse(tester.regexBond("( 0 1 7 )"));
	}

	@Test
	public void testregexBond_207()
	throws Exception{
	assertFalse(tester.regexBond("( 0 2 ("));
	}

	@Test
	public void testregexBond_208()
	throws Exception{
	assertFalse(tester.regexBond("( 0 2 )"));
	}

	@Test
	public void testregexBond_209()
	throws Exception{
	assertTrue(tester.regexBond("( 0 2 0 0 7 )"));
	}

	@Test
	public void testregexBond_210()
	throws Exception{
	assertFalse(tester.regexBond("( 0 2 0 7 )"));
	}

	@Test
	public void testregexBond_211()
	throws Exception{
	assertFalse(tester.regexBond("( 0 2 7 )"));
	}

	@Test
	public void testregexBond_212()
	throws Exception{
	assertFalse(tester.regexBond("( 0 3 ("));
	}

	@Test
	public void testregexBond_213()
	throws Exception{
	assertFalse(tester.regexBond("( 0 3 )"));
	}

	@Test
	public void testregexBond_214()
	throws Exception{
	assertTrue(tester.regexBond("( 0 3 0 0 7 )"));
	}

	@Test
	public void testregexBond_215()
	throws Exception{
	assertFalse(tester.regexBond("( 0 3 0 7 )"));
	}

	@Test
	public void testregexBond_216()
	throws Exception{
	assertFalse(tester.regexBond("( 0 3 7 )"));
	}

	@Test
	public void testregexBond_217()
	throws Exception{
	assertFalse(tester.regexBond("( 0 4 ("));
	}

	@Test
	public void testregexBond_218()
	throws Exception{
	assertFalse(tester.regexBond("( 0 4 )"));
	}

	@Test
	public void testregexBond_219()
	throws Exception{
	assertTrue(tester.regexBond("( 0 4 0 0 7 )"));
	}

	@Test
	public void testregexBond_220()
	throws Exception{
	assertFalse(tester.regexBond("( 0 4 0 7 )"));
	}

	@Test
	public void testregexBond_221()
	throws Exception{
	assertFalse(tester.regexBond("( 0 4 7 )"));
	}

	@Test
	public void testregexBond_222()
	throws Exception{
	assertFalse(tester.regexBond("( 0 5 ("));
	}

	@Test
	public void testregexBond_223()
	throws Exception{
	assertFalse(tester.regexBond("( 0 5 )"));
	}

	@Test
	public void testregexBond_224()
	throws Exception{
	assertTrue(tester.regexBond("( 0 5 0 0 7 )"));
	}

	@Test
	public void testregexBond_225()
	throws Exception{
	assertFalse(tester.regexBond("( 0 5 0 7 )"));
	}

	@Test
	public void testregexBond_226()
	throws Exception{
	assertFalse(tester.regexBond("( 0 5 7 )"));
	}

	@Test
	public void testregexBond_227()
	throws Exception{
	assertFalse(tester.regexBond("( 0 6 ("));
	}

	@Test
	public void testregexBond_228()
	throws Exception{
	assertFalse(tester.regexBond("( 0 6 )"));
	}

	@Test
	public void testregexBond_229()
	throws Exception{
	assertTrue(tester.regexBond("( 0 6 0 0 7 )"));
	}

	@Test
	public void testregexBond_230()
	throws Exception{
	assertFalse(tester.regexBond("( 0 6 0 7 )"));
	}

	@Test
	public void testregexBond_231()
	throws Exception{
	assertFalse(tester.regexBond("( 0 6 7 )"));
	}

	@Test
	public void testregexBond_232()
	throws Exception{
	assertFalse(tester.regexBond("( 0 7 ("));
	}

	@Test
	public void testregexBond_233()
	throws Exception{
	assertTrue(tester.regexBond("( 0 7 0 0 7 )"));
	}

	@Test
	public void testregexBond_234()
	throws Exception{
	assertFalse(tester.regexBond("( 0 7 0 7 )"));
	}

	@Test
	public void testregexBond_235()
	throws Exception{
	assertFalse(tester.regexBond("( 0 7 7 )"));
	}

	@Test
	public void testregexBond_236()
	throws Exception{
	assertFalse(tester.regexBond("( 0 8 ("));
	}

	@Test
	public void testregexBond_237()
	throws Exception{
	assertFalse(tester.regexBond("( 0 8 )"));
	}

	@Test
	public void testregexBond_238()
	throws Exception{
	assertTrue(tester.regexBond("( 0 8 0 0 7 )"));
	}

	@Test
	public void testregexBond_239()
	throws Exception{
	assertFalse(tester.regexBond("( 0 8 0 7 )"));
	}

	@Test
	public void testregexBond_240()
	throws Exception{
	assertFalse(tester.regexBond("( 0 8 7 )"));
	}

	@Test
	public void testregexBond_241()
	throws Exception{
	assertFalse(tester.regexBond("( 0 9 ("));
	}

	@Test
	public void testregexBond_242()
	throws Exception{
	assertFalse(tester.regexBond("( 0 9 )"));
	}

	@Test
	public void testregexBond_243()
	throws Exception{
	assertTrue(tester.regexBond("( 0 9 0 0 7 )"));
	}

	@Test
	public void testregexBond_244()
	throws Exception{
	assertFalse(tester.regexBond("( 0 9 0 7 )"));
	}

	@Test
	public void testregexBond_245()
	throws Exception{
	assertFalse(tester.regexBond("( 0 9 7 )"));
	}

	@Test
	public void testregexBond_246()
	throws Exception{
	assertFalse(tester.regexBond("( 1 ("));
	}

	@Test
	public void testregexBond_247()
	throws Exception{
	assertFalse(tester.regexBond("( 1 )"));
	}

	@Test
	public void testregexBond_248()
	throws Exception{
	assertTrue(tester.regexBond("( 1 0 0 7 )"));
	}

	@Test
	public void testregexBond_249()
	throws Exception{
	assertFalse(tester.regexBond("( 1 0 7 )"));
	}

	@Test
	public void testregexBond_250()
	throws Exception{
	assertFalse(tester.regexBond("( 1 7 )"));
	}

	@Test
	public void testregexBond_251()
	throws Exception{
	assertFalse(tester.regexBond("( 2 ("));
	}

	@Test
	public void testregexBond_252()
	throws Exception{
	assertFalse(tester.regexBond("( 2 )"));
	}

	@Test
	public void testregexBond_253()
	throws Exception{
	assertTrue(tester.regexBond("( 2 0 0 7 )"));
	}

	@Test
	public void testregexBond_254()
	throws Exception{
	assertFalse(tester.regexBond("( 2 0 7 )"));
	}

	@Test
	public void testregexBond_255()
	throws Exception{
	assertFalse(tester.regexBond("( 2 7 )"));
	}

	@Test
	public void testregexBond_256()
	throws Exception{
	assertFalse(tester.regexBond("( 3 ("));
	}

	@Test
	public void testregexBond_257()
	throws Exception{
	assertFalse(tester.regexBond("( 3 )"));
	}

	@Test
	public void testregexBond_258()
	throws Exception{
	assertTrue(tester.regexBond("( 3 0 0 7 )"));
	}

	@Test
	public void testregexBond_259()
	throws Exception{
	assertFalse(tester.regexBond("( 3 0 7 )"));
	}

	@Test
	public void testregexBond_260()
	throws Exception{
	assertFalse(tester.regexBond("( 3 7 )"));
	}

	@Test
	public void testregexBond_261()
	throws Exception{
	assertFalse(tester.regexBond("( 4 ("));
	}

	@Test
	public void testregexBond_262()
	throws Exception{
	assertFalse(tester.regexBond("( 4 )"));
	}

	@Test
	public void testregexBond_263()
	throws Exception{
	assertTrue(tester.regexBond("( 4 0 0 7 )"));
	}

	@Test
	public void testregexBond_264()
	throws Exception{
	assertFalse(tester.regexBond("( 4 0 7 )"));
	}

	@Test
	public void testregexBond_265()
	throws Exception{
	assertFalse(tester.regexBond("( 4 7 )"));
	}

	@Test
	public void testregexBond_266()
	throws Exception{
	assertFalse(tester.regexBond("( 5 ("));
	}

	@Test
	public void testregexBond_267()
	throws Exception{
	assertFalse(tester.regexBond("( 5 )"));
	}

	@Test
	public void testregexBond_268()
	throws Exception{
	assertTrue(tester.regexBond("( 5 0 0 7 )"));
	}

	@Test
	public void testregexBond_269()
	throws Exception{
	assertFalse(tester.regexBond("( 5 0 7 )"));
	}

	@Test
	public void testregexBond_270()
	throws Exception{
	assertFalse(tester.regexBond("( 5 7 )"));
	}

	@Test
	public void testregexBond_271()
	throws Exception{
	assertFalse(tester.regexBond("( 6 ("));
	}

	@Test
	public void testregexBond_272()
	throws Exception{
	assertFalse(tester.regexBond("( 6 )"));
	}

	@Test
	public void testregexBond_273()
	throws Exception{
	assertTrue(tester.regexBond("( 6 0 0 7 )"));
	}

	@Test
	public void testregexBond_274()
	throws Exception{
	assertFalse(tester.regexBond("( 6 0 7 )"));
	}

	@Test
	public void testregexBond_275()
	throws Exception{
	assertFalse(tester.regexBond("( 6 7 )"));
	}

	@Test
	public void testregexBond_276()
	throws Exception{
	assertFalse(tester.regexBond("( 7 ("));
	}

	@Test
	public void testregexBond_277()
	throws Exception{
	assertTrue(tester.regexBond("( 7 0 0 7 )"));
	}

	@Test
	public void testregexBond_278()
	throws Exception{
	assertFalse(tester.regexBond("( 7 0 7 )"));
	}

	@Test
	public void testregexBond_279()
	throws Exception{
	assertFalse(tester.regexBond("( 7 7 )"));
	}

	@Test
	public void testregexBond_280()
	throws Exception{
	assertFalse(tester.regexBond("( 8 ("));
	}

	@Test
	public void testregexBond_281()
	throws Exception{
	assertFalse(tester.regexBond("( 8 )"));
	}

	@Test
	public void testregexBond_282()
	throws Exception{
	assertTrue(tester.regexBond("( 8 0 0 7 )"));
	}

	@Test
	public void testregexBond_283()
	throws Exception{
	assertFalse(tester.regexBond("( 8 0 7 )"));
	}

	@Test
	public void testregexBond_284()
	throws Exception{
	assertFalse(tester.regexBond("( 8 7 )"));
	}

	@Test
	public void testregexBond_285()
	throws Exception{
	assertFalse(tester.regexBond("( 9 ("));
	}

	@Test
	public void testregexBond_286()
	throws Exception{
	assertFalse(tester.regexBond("( 9 )"));
	}

	@Test
	public void testregexBond_287()
	throws Exception{
	assertTrue(tester.regexBond("( 9 0 0 7 )"));
	}

	@Test
	public void testregexBond_288()
	throws Exception{
	assertFalse(tester.regexBond("( 9 0 7 )"));
	}

	@Test
	public void testregexBond_289()
	throws Exception{
	assertFalse(tester.regexBond("( 9 7 )"));
	}

	@Test
	public void testregexBond_290()
	throws Exception{
	assertFalse(tester.regexBond(") ("));
	}

	@Test
	public void testregexBond_291()
	throws Exception{
	assertFalse(tester.regexBond(") )"));
	}

	@Test
	public void testregexBond_292()
	throws Exception{
	assertFalse(tester.regexBond(") 0 0 7 )"));
	}

	@Test
	public void testregexBond_293()
	throws Exception{
	assertFalse(tester.regexBond(") 0 7 )"));
	}

	@Test
	public void testregexBond_294()
	throws Exception{
	assertFalse(tester.regexBond(") 7 )"));
	}

	@Test
	public void testregexBond_295()
	throws Exception{
	assertFalse(tester.regexBond("0 ("));
	}

	@Test
	public void testregexBond_296()
	throws Exception{
	assertFalse(tester.regexBond("0 )"));
	}

	@Test
	public void testregexBond_297()
	throws Exception{
	assertFalse(tester.regexBond("0 0 0 7 )"));
	}

	@Test
	public void testregexBond_298()
	throws Exception{
	assertFalse(tester.regexBond("0 0 7 )"));
	}

	@Test
	public void testregexBond_299()
	throws Exception{
	assertFalse(tester.regexBond("0 7 )"));
	}

	@Test
	public void testregexBond_300()
	throws Exception{
	assertFalse(tester.regexBond("1 ("));
	}

	@Test
	public void testregexBond_301()
	throws Exception{
	assertFalse(tester.regexBond("1 )"));
	}

	@Test
	public void testregexBond_302()
	throws Exception{
	assertFalse(tester.regexBond("1 0 0 7 )"));
	}

	@Test
	public void testregexBond_303()
	throws Exception{
	assertFalse(tester.regexBond("1 0 7 )"));
	}

	@Test
	public void testregexBond_304()
	throws Exception{
	assertFalse(tester.regexBond("1 7 )"));
	}

	@Test
	public void testregexBond_305()
	throws Exception{
	assertFalse(tester.regexBond("2 ("));
	}

	@Test
	public void testregexBond_306()
	throws Exception{
	assertFalse(tester.regexBond("2 )"));
	}

	@Test
	public void testregexBond_307()
	throws Exception{
	assertFalse(tester.regexBond("2 0 0 7 )"));
	}

	@Test
	public void testregexBond_308()
	throws Exception{
	assertFalse(tester.regexBond("2 0 7 )"));
	}

	@Test
	public void testregexBond_309()
	throws Exception{
	assertFalse(tester.regexBond("2 7 )"));
	}

	@Test
	public void testregexBond_310()
	throws Exception{
	assertFalse(tester.regexBond("3 ("));
	}

	@Test
	public void testregexBond_311()
	throws Exception{
	assertFalse(tester.regexBond("3 )"));
	}

	@Test
	public void testregexBond_312()
	throws Exception{
	assertFalse(tester.regexBond("3 0 0 7 )"));
	}

	@Test
	public void testregexBond_313()
	throws Exception{
	assertFalse(tester.regexBond("3 0 7 )"));
	}

	@Test
	public void testregexBond_314()
	throws Exception{
	assertFalse(tester.regexBond("3 7 )"));
	}

	@Test
	public void testregexBond_315()
	throws Exception{
	assertFalse(tester.regexBond("4 ("));
	}

	@Test
	public void testregexBond_316()
	throws Exception{
	assertFalse(tester.regexBond("4 )"));
	}

	@Test
	public void testregexBond_317()
	throws Exception{
	assertFalse(tester.regexBond("4 0 0 7 )"));
	}

	@Test
	public void testregexBond_318()
	throws Exception{
	assertFalse(tester.regexBond("4 0 7 )"));
	}

	@Test
	public void testregexBond_319()
	throws Exception{
	assertFalse(tester.regexBond("4 7 )"));
	}

	@Test
	public void testregexBond_320()
	throws Exception{
	assertFalse(tester.regexBond("5 ("));
	}

	@Test
	public void testregexBond_321()
	throws Exception{
	assertFalse(tester.regexBond("5 )"));
	}

	@Test
	public void testregexBond_322()
	throws Exception{
	assertFalse(tester.regexBond("5 0 0 7 )"));
	}

	@Test
	public void testregexBond_323()
	throws Exception{
	assertFalse(tester.regexBond("5 0 7 )"));
	}

	@Test
	public void testregexBond_324()
	throws Exception{
	assertFalse(tester.regexBond("5 7 )"));
	}

	@Test
	public void testregexBond_325()
	throws Exception{
	assertFalse(tester.regexBond("6 ("));
	}

	@Test
	public void testregexBond_326()
	throws Exception{
	assertFalse(tester.regexBond("6 )"));
	}

	@Test
	public void testregexBond_327()
	throws Exception{
	assertFalse(tester.regexBond("6 0 0 7 )"));
	}

	@Test
	public void testregexBond_328()
	throws Exception{
	assertFalse(tester.regexBond("6 0 7 )"));
	}

	@Test
	public void testregexBond_329()
	throws Exception{
	assertFalse(tester.regexBond("6 7 )"));
	}

	@Test
	public void testregexBond_330()
	throws Exception{
	assertFalse(tester.regexBond("7 ("));
	}

	@Test
	public void testregexBond_331()
	throws Exception{
	assertFalse(tester.regexBond("7 )"));
	}

	@Test
	public void testregexBond_332()
	throws Exception{
	assertFalse(tester.regexBond("7 0 0 7 )"));
	}

	@Test
	public void testregexBond_333()
	throws Exception{
	assertFalse(tester.regexBond("7 0 7 )"));
	}

	@Test
	public void testregexBond_334()
	throws Exception{
	assertFalse(tester.regexBond("7 7 )"));
	}

	@Test
	public void testregexBond_335()
	throws Exception{
	assertFalse(tester.regexBond("8 ("));
	}

	@Test
	public void testregexBond_336()
	throws Exception{
	assertFalse(tester.regexBond("8 )"));
	}

	@Test
	public void testregexBond_337()
	throws Exception{
	assertFalse(tester.regexBond("8 0 0 7 )"));
	}

	@Test
	public void testregexBond_338()
	throws Exception{
	assertFalse(tester.regexBond("8 0 7 )"));
	}

	@Test
	public void testregexBond_339()
	throws Exception{
	assertFalse(tester.regexBond("8 7 )"));
	}

	@Test
	public void testregexBond_340()
	throws Exception{
	assertFalse(tester.regexBond("9 ("));
	}

	@Test
	public void testregexBond_341()
	throws Exception{
	assertFalse(tester.regexBond("9 )"));
	}

	@Test
	public void testregexBond_342()
	throws Exception{
	assertFalse(tester.regexBond("9 0 0 7 )"));
	}

	@Test
	public void testregexBond_343()
	throws Exception{
	assertFalse(tester.regexBond("9 0 7 )"));
	}

	@Test
	public void testregexBond_344()
	throws Exception{
	assertFalse(tester.regexBond("9 7 )"));
	}

	@Test
	public void testregexBond_345()
	throws Exception{
	assertFalse(tester.regexBond("("));
	}

	@Test
	public void testregexBond_346()
	throws Exception{
	assertFalse(tester.regexBond(")"));
	}





}
